import { OverDraftLoanAccount } from './index';

export function validateWithdrawAmount(
  amountRequested: number,
  overDraftAccount: OverDraftLoanAccount
): boolean {
  if (amountRequested > overDraftAccount.availableCredit) {
    throw Error(
      'Cannot withdraw as requested amount is greater than the available credit'
    );
  }

  return true;
}
