import { differenceInDays, addDays, addMonths } from 'date-fns';
import { calculateInterest } from './calculator';
import numeral from 'numeral';
import groupBy from 'lodash.groupby';

import { validateWithdrawAmount } from './validator';

export type EVENT_TYPES =
  | 'CREATE_LOAN_ACCOUNT'
  | 'WITHDRAWAL'
  | 'PAYMENT'
  | 'RENEW'
  | 'INTEREST_CUT_WINDOW_SURPASSED'
  | 'INTEREST_ACCRUAL'
  | 'PENALTY_INTEREST_ACCRUAL'
  | 'ACCOUNT_RENEW_DATE_SURPASSED';

export interface Transaction {
  createdAt: Date;
  type: EVENT_TYPES;
  amount: number;
  accruedInterest?: number;
  resultingLoanBalance: number;
}

export interface OverDraftLoanAccount {
  creditLimit: number;
  interestRate: number;
  penaltyRate: number;
  availableCredit: number;
  currentBalance: number;
  totalAccruedInterest: number;
  interestCutWindow: number; // IN MONTHS
  interestCutDates: Date[];
  accountTenure: number; // IN MONTHS
  accountRenewDate: Date;
  events: Transaction[];
  createdAt: Date;
}

export function createTransaction({
  createdAt = new Date(),
  type,
  amount,
  accruedInterest = 0,
  resultingLoanBalance
}: {
  createdAt: Date;
  type: EVENT_TYPES;
  amount: number;
  accruedInterest?: number;
  resultingLoanBalance: number;
}): Transaction {
  return {
    createdAt,
    type,
    amount,
    accruedInterest,
    resultingLoanBalance
  };
}

export function createOverDraftAccount({
  creditLimit = 50000,
  interestRate = 5.0 / 100,
  penaltyRate = 2.0 / 100,
  interestCutWindow = 3, // IN MONTHS
  accountTenure = 12, // IN MONTHS
  createdAt = new Date()
}: {
  creditLimit: number;
  interestRate: number;
  penaltyRate: number;
  interestCutWindow: number;
  accountTenure: number;
  createdAt?: Date;
}): OverDraftLoanAccount {
  const interestCutDates = [];
  const accountRenewDate = addMonths(createdAt, accountTenure);
  for (let i = 1; ; i++) {
    const interestCutDate = addMonths(createdAt, interestCutWindow * i);
    if (interestCutDate <= accountRenewDate)
      interestCutDates.push(interestCutDate);
    else break;
  }

  return {
    creditLimit,
    interestRate: Number(numeral(interestRate / 100).format('0.00000')),
    penaltyRate: Number(numeral(penaltyRate / 100).format('0.00000')),
    availableCredit: creditLimit,
    currentBalance: 0,
    totalAccruedInterest: 0,
    interestCutWindow,
    interestCutDates,
    accountTenure,
    accountRenewDate,
    events: [],
    createdAt
  };
}

export function renewOverDraftAccount(
  account: OverDraftLoanAccount,
  newAccountTenure: number
): OverDraftLoanAccount {
  account.accountTenure += newAccountTenure;
  account.accountRenewDate = addMonths(
    account.accountRenewDate,
    newAccountTenure
  );

  return account;
}

export function withdraw(
  amountRequested: number,
  overDraftAccount: OverDraftLoanAccount,
  withdrawDate = new Date()
): OverDraftLoanAccount {
  if (!validateWithdrawAmount(amountRequested, overDraftAccount)) {
    console.error('Cannot proceed with the loan request as validation failed');
  }

  const transaction = createTransaction({
    createdAt: withdrawDate,
    type: 'WITHDRAWAL',
    amount: amountRequested,
    resultingLoanBalance: Number(
      numeral(overDraftAccount.currentBalance + amountRequested).format(
        '0.00000'
      )
    )
  });
  overDraftAccount.events.push(transaction);

  overDraftAccount.availableCredit = Number(
    numeral(overDraftAccount.availableCredit - amountRequested).format(
      '0.00000'
    )
  );
  overDraftAccount.currentBalance = transaction.resultingLoanBalance;

  return overDraftAccount;
}

export function pay(
  paymentAmount: number,
  overDraftAccount: OverDraftLoanAccount,
  payDate = new Date()
): OverDraftLoanAccount {
  const transaction = createTransaction({
    createdAt: payDate,
    type: 'PAYMENT',
    amount: -paymentAmount,
    resultingLoanBalance: Number(
      numeral(overDraftAccount.currentBalance - paymentAmount).format('0.00000')
    )
  });
  overDraftAccount.events.push(transaction);

  overDraftAccount.currentBalance = transaction.resultingLoanBalance;
  overDraftAccount.availableCredit = Number(
    numeral(overDraftAccount.availableCredit + paymentAmount).format('0.00000')
  );

  return overDraftAccount;
}

export function getSummary(
  overDraftAccount: OverDraftLoanAccount,
  date = new Date()
): OverDraftLoanAccount {
  overDraftAccount.events.sort((a, b) => (a > b ? 0 : -1));

  const numberOfTransactions = overDraftAccount.events.length;
  for (let i = 1; i < numberOfTransactions; i++) {
    overDraftAccount.totalAccruedInterest += calculateInterest({
      currentBalance: overDraftAccount.events[i - 1].resultingLoanBalance,
      interestRatePerYear: overDraftAccount.interestRate,
      time: differenceInDays(
        overDraftAccount.events[i].createdAt,
        overDraftAccount.events[i - 1].createdAt
      )
    });
  }

  overDraftAccount.totalAccruedInterest += calculateInterest({
    currentBalance:
      overDraftAccount.events[numberOfTransactions - 1].resultingLoanBalance,
    interestRatePerYear: overDraftAccount.interestRate,
    time: differenceInDays(
      overDraftAccount.events[numberOfTransactions - 1].createdAt,
      date
    )
  });

  return overDraftAccount;
}

export function createLedger(
  overDraftAccount: OverDraftLoanAccount,
  date = new Date()
): { ledger: Transaction[]; account: OverDraftLoanAccount } {
  const ledger = [];
  const startDate = overDraftAccount.createdAt;
  const groupedEvents = groupBy(
    overDraftAccount.events.sort((a, b) => (a > b ? 0 : -1)),
    transaction => transaction.createdAt.toLocaleDateString()
  );
  const formattedInterestCutDates = overDraftAccount.interestCutDates.map(
    date => date.toLocaleDateString()
  );

  ledger.push(
    createTransaction({
      createdAt: startDate,
      type: 'CREATE_LOAN_ACCOUNT',
      amount: 0,
      resultingLoanBalance: 0
    })
  );
  overDraftAccount.currentBalance = 0;
  overDraftAccount.totalAccruedInterest = 0;
  // eslint-disable-next-line no-unmodified-loop-condition
  for (let i = startDate; i <= date; i = addDays(i, 1)) {
    const formattedDate = i.toLocaleDateString();

    if (formattedInterestCutDates.includes(formattedDate)) {
      if (overDraftAccount.totalAccruedInterest > 0) {
        overDraftAccount.currentBalance +=
          overDraftAccount.totalAccruedInterest;
        overDraftAccount.availableCredit -=
          overDraftAccount.totalAccruedInterest;

        ledger.push(
          createTransaction({
            createdAt: i,
            type: 'INTEREST_CUT_WINDOW_SURPASSED',
            amount: 0,
            accruedInterest: 0,
            resultingLoanBalance: overDraftAccount.currentBalance
          })
        );
      }
    }

    if (groupedEvents[formattedDate]) {
      groupedEvents[formattedDate].forEach(event => {
        overDraftAccount.currentBalance += event.amount;
        ledger.push(event);
      });
    }

    let currentAccruedInterest = 0;
    if (overDraftAccount.accountRenewDate > i) {
      currentAccruedInterest = calculateInterest({
        currentBalance: overDraftAccount.currentBalance,
        interestRatePerYear:
          overDraftAccount.interestRate + overDraftAccount.penaltyRate,
        time: 1
      });

      ledger.push(
        createTransaction({
          createdAt: i,
          type: 'PENALTY_INTEREST_ACCRUAL',
          amount: 0,
          accruedInterest: currentAccruedInterest,
          resultingLoanBalance: 0
        })
      );
    } else {
      currentAccruedInterest = calculateInterest({
        currentBalance: overDraftAccount.currentBalance,
        interestRatePerYear: overDraftAccount.interestRate,
        time: 1
      });

      ledger.push(
        createTransaction({
          createdAt: i,
          type: 'INTEREST_ACCRUAL',
          amount: 0,
          accruedInterest: currentAccruedInterest,
          resultingLoanBalance: 0
        })
      );
    }

    if (overDraftAccount.currentBalance > overDraftAccount.creditLimit) {
      const penaltyInterest = calculateInterest({
        currentBalance:
          overDraftAccount.creditLimit - overDraftAccount.currentBalance,
        interestRatePerYear:
          overDraftAccount.interestRate + overDraftAccount.penaltyRate,
        time: 1
      });
      currentAccruedInterest += penaltyInterest;

      ledger.push(
        createTransaction({
          createdAt: i,
          type: 'PENALTY_INTEREST_ACCRUAL',
          amount: 0,
          accruedInterest: penaltyInterest,
          resultingLoanBalance: 0
        })
      );
    }

    overDraftAccount.totalAccruedInterest += currentAccruedInterest;
  }

  return { ledger, account: overDraftAccount };
}

const account = createOverDraftAccount({
  creditLimit: 15000,
  interestRate: 12,
  penaltyRate: 3,
  accountTenure: 12,
  interestCutWindow: 3
});

withdraw(5000, account, new Date());
pay(2000, account, new Date(2020, 1, 26));
console.info(
  JSON.stringify(createLedger(account, new Date(2020, 2, 5)), null, 4)
);
