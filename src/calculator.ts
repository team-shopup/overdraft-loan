import numeral from 'numeral';

export function calculateInterest({
  currentBalance,
  interestRatePerYear,
  time,
  daysInYear = 360
}: {
  currentBalance: number;
  interestRatePerYear: number;
  time: number;
  daysInYear?: number;
}): number {
  return Number(
    numeral(currentBalance * interestRatePerYear * (time / daysInYear)).format(
      '0.00000'
    )
  );
}
